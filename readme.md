# BlogBackupTool
博客园备份工具
## 介绍
从博客园下载的备份只是一个xml，我想把每一篇博客单独形成一个文件，便是做出了这个工具，之前弄的是命令行版本的，这次做出了图形化界面的工具，希望大家能够多多支持！	
![](https://images.gitee.com/uploads/images/2019/0915/101211_4edbe3ad_5050769.png)

## 功能介绍
- MD和Html分类功能
- 标签分类功能
- 导航页面生成	

## 使用
需要安装jdk1.8以上，下载`BlogBackupTool.zip`，解压，双击`BlogBackupTool.jar`即可打开程序

[点击下载BlogBackupTool.zip](https://gitee.com/stars-one/BlogBackupTool/repository/archive/v4.0?format=zip)